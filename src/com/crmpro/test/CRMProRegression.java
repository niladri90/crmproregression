package com.crmpro.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.crmpro.pages.Login;
import com.crmpro.pages.RegistrationPage;
import com.crmpro.utility.Browserfactory;

public class CRMProRegression {

	WebDriver driver;
	Login objlogin;
	RegistrationPage objregistration;

	@BeforeMethod
	public void checkapplaunch() {

		driver = Browserfactory.applaunch("Chrome", "https://www.freecrm.com/index.html");
	}

	@Test(priority = 1,enabled= true)

	public void checkCursorinUsername() throws InterruptedException {
		// RegistrationPage register = PageFactory.initElements(driver,
		// RegistrationPage.class);
		// register.verifyregistrationdetails();

		objlogin = PageFactory.initElements(driver, Login.class);
		Assert.assertEquals(objlogin.checkcursorusernamefield(), true);

	}

	@Test(priority = 2,enabled= true)

	public void checkEnteredUsername() throws InterruptedException {

		objlogin = PageFactory.initElements(driver, Login.class);
		Assert.assertEquals(objlogin.checkEnteredUsernameField(), true);

	}

	@Test(priority = 3,enabled= true)
	public void checkCursorinPassword() throws InterruptedException {

		objlogin = PageFactory.initElements(driver, Login.class);
		Assert.assertEquals(objlogin.checkCursorpasswordfield(), true);
	}

	@Test(priority = 4,enabled= true)

	public void checkEnteredPassword() throws InterruptedException {

		objlogin = PageFactory.initElements(driver, Login.class);
		Assert.assertEquals(objlogin.checkEnteredPasswordfield(), true);
	}

	@Test(priority = 5,enabled= true)

	public void blankFieldChecking() throws InterruptedException {

		objlogin = PageFactory.initElements(driver, Login.class);
		Assert.assertEquals(objlogin.checkBlankField(), true);

	}

	@Test(priority = 6,enabled= true)

	public void invalidLogin() throws InterruptedException {
		objlogin = PageFactory.initElements(driver, Login.class);
		Assert.assertEquals(objlogin.checkInvalidLogin(), true);
	}

	@Test(priority = 7,enabled= true)

	public void validLogin() throws InterruptedException {

		objlogin = PageFactory.initElements(driver, Login.class);
		Assert.assertEquals(objlogin.checkvalidLogin(), true);

	}

	@Test(priority = 8,enabled= true)
	public void checkFirstname() throws InterruptedException{
		objregistration = PageFactory.initElements(driver, RegistrationPage.class);
		Assert.assertEquals(objregistration.checkFirstNameField(),true);
	}
	
	@Test(priority = 9,enabled= true)
	public void checkblankFirstname() throws InterruptedException{
		objregistration = PageFactory.initElements(driver, RegistrationPage.class);
		Assert.assertEquals(objregistration.checkblankFirstNameField(),true);
	}

	@AfterMethod

	public void checkclosedriver() {

		Browserfactory.driverclose();
	}

}
