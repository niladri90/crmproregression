package com.crmpro.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Login {

	WebDriver driver;

	@FindBy(xpath = "//input[@placeholder='Username']")
	WebElement username;

	@FindBy(xpath = "//input[@placeholder='Password']")
	WebElement password;

	@FindBy(xpath = "//input[@value='Login']")
	WebElement login;

	public Login(WebDriver driver) {

		this.driver = driver;
	}

	public boolean checkcursorusernamefield() throws InterruptedException {

		Thread.sleep(2000);
		
    //Click on the username field
		
		username.click();

		if (username.isEnabled()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean checkEnteredUsernameField() throws InterruptedException {

		Thread.sleep(2000);
		username.click();
		username.sendKeys("testcrm100");

		String textInsideInputBox = username.getAttribute("value");

		if (textInsideInputBox.isEmpty()) {
			System.out.println("Input field is empty");
			return false;
		} else {

			System.out.println("Entered username is " + textInsideInputBox);

			return true;
		}

	}

	public boolean checkCursorpasswordfield() throws InterruptedException {

		Thread.sleep(2000);

		password.click();

		if (password.isEnabled()) {
			return true;

		} else {
			return false;
		}
	}

	public boolean checkEnteredPasswordfield() throws InterruptedException {

		Thread.sleep(2000);
		password.click();
		password.sendKeys("123456");

		String textInsideInputBox = password.getAttribute("value");

		if (textInsideInputBox.isEmpty()) {
			System.out.println("Input field is empty");
			return false;
		} else {

			System.out.println("Entered password is " + textInsideInputBox);

			return true;
		}

	}

	public boolean checkBlankField() throws InterruptedException {

		Thread.sleep(2000);
		username.click();

		username.sendKeys("");

		password.click();
		password.sendKeys("");

		login.click();

		Thread.sleep(3000);

		String expecxted = "#1 Free CRM software in the cloud for sales and service";
		String actualtitle = driver.getTitle();

		if (expecxted.equals(actualtitle)) {
			System.out.println("Page is refreshed successfully");
		} else {

			System.out.println("Page is not refreshed successfully");

		}

		String textInsideusernameInputBox = username.getAttribute("value");

		if (textInsideusernameInputBox.isEmpty()) {

			System.out.println("Input (Username & Password) field is empty");

			return true;
		} else {

			System.out.println("Entered username is " + textInsideusernameInputBox);

			return false;
		}

	}

	public boolean checkInvalidLogin() throws InterruptedException {

		Thread.sleep(2000);
		username.click();

		username.sendKeys("testcrm1343");

		password.click();
		password.sendKeys("1234567890");

		login.click();

		Thread.sleep(3000);

		String expecxted = "#1 Free CRM software in the cloud for sales and service";
		String actualtitle = driver.getTitle();

		if (expecxted.equals(actualtitle)) {
			System.out.println("Page is refreshed successfully");
		} else {

			System.out.println("Page is not refreshed successfully");

		}

		String textInsidepasswordInputBox = password.getAttribute("value");

		if (textInsidepasswordInputBox.isEmpty()) {

			System.out.println("Input (Username & Password) field is empty");

			return true;
		} else {

			System.out.println("Entered username is " + textInsidepasswordInputBox);

			return false;
		}
	}

	public boolean checkvalidLogin() throws InterruptedException {

		Thread.sleep(2000);
		username.click();

		username.sendKeys("testcrm100");

		password.click();
		password.sendKeys("123456");

		login.click();

		String expectedtitle = "CRMPRO";

		String actualtitle = driver.getTitle();

		if (expectedtitle.equals(actualtitle)) {

			return true;
		} else {

			return false;
		}
	}
}
