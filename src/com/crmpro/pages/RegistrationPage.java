package com.crmpro.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegistrationPage {

	WebDriver driver;
	WebDriverWait wait;
	@FindBy(xpath = "//input[@placeholder='First Name']")
	WebElement firstname;

	@FindBy(xpath = "//input[@placeholder='Last Name']")
	WebElement lastname;

	@FindBy(xpath = "//font[@color='red']")
	WebElement signup;

	@FindBy(xpath = "//input[@value='false']")
	WebElement checkbox;

	@FindBy(xpath = "//button[@id='submitButton']")
	WebElement submit;

	@FindBy(xpath = "//small[contains(text(),'Please enter your first name')]")
	WebElement firstnamealerttext;

	public RegistrationPage(WebDriver driver) {

		this.driver = driver;
	}

	public boolean checkFirstNameField() throws InterruptedException {

		Thread.sleep(2000);
		signup.click();

		wait = new WebDriverWait(driver, 3000);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='First Name']")));

		firstname.click();
		firstname.sendKeys("Niladri");

		String textInsidefirstnameInputBox = firstname.getAttribute("value");

		if (textInsidefirstnameInputBox.isEmpty()) {
			System.out.println("Input field is empty");
			return false;
		} else {

			System.out.println("Entered Firstname is " + textInsidefirstnameInputBox);

			return true;
		}
	}

	public boolean checkblankFirstNameField() throws InterruptedException {

		Thread.sleep(2000);
		signup.click();

		wait = new WebDriverWait(driver, 3000);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='First Name']")));

		firstname.click();

		checkbox.click();

		submit.click();

		Thread.sleep(2000);

		String expectedalerttext = "Please enter your first name";
		String actualalerttext = firstnamealerttext.getText();

		System.out.println(actualalerttext);

		if (expectedalerttext.equals(actualalerttext)) {

			System.out.println("Tesst case passed successfully");
			return true;
		}

		else {

			System.out.println("Tesst case failed");
			return false;
		}
	}
}
