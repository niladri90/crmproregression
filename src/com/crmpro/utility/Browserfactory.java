package com.crmpro.utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Browserfactory {

	static WebDriver driver;

	public static WebDriver applaunch(String browsername, String url) {

		if (browsername.equalsIgnoreCase("firefox")) {

			driver = new FirefoxDriver();
		} else if (browsername.equalsIgnoreCase("Chrome")) {

			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
		}
		driver.manage().window().maximize();

		driver.get(url);

		return driver;
	}
	
	public static void driverclose() {
		
		driver.close();
	}
}
